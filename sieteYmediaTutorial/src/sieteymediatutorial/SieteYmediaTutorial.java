/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sieteymediatutorial;

import baraja.Baraja;
import baraja.Naipe;
import java.util.Scanner;

/**
 *
 * @author Cesid
 */
public class SieteYmediaTutorial {
    
    //Método static para preguntar si se planta o no (Validando respuesta)
    public static char plantarse() {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("¿Te plantas (s/n)? : ");
        char respuesta = sc.nextLine().charAt(0);
	respuesta = Character.toUpperCase(respuesta);
        
        while (respuesta != 'S' && respuesta != 'N') {
            System.out.print("ERROR, introduzca s o n: ");
            respuesta = sc.nextLine().charAt(0);
            respuesta = Character.toUpperCase(respuesta); 
        }
        return respuesta;
    }
    
    //Método static para preguntar si quiere ocultar la carta o no (Validando respuesta)
    public static char ocultarCarta() {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("¿Deseas ocultar esta carta (s/n)? : ");
        char respuesta = sc.nextLine().charAt(0);
	respuesta = Character.toUpperCase(respuesta);
        
        while (respuesta != 'S' && respuesta != 'N') {
            System.out.print("ERROR, introduzca s o n: ");
            respuesta = sc.nextLine().charAt(0);
            respuesta = Character.toUpperCase(respuesta); 
        }
        return respuesta;
    }
    
    /**
     * @param args the command line arguments
     */
    //Método Main
    public static void main(String[] args) {
        
        //Creamos baraja
        Baraja b = new Baraja(false); //Sin 8 y 9
        
        //Ponemos valor a cada naipe
        for (int palo = Baraja.OROS;  palo <= Baraja.BASTOS; palo++) {
            for (int numero = 1; numero <= 10; numero++) {
                Naipe n = b.getNaipe(palo, numero);
                if (numero <= 7) {
                    n.setValor(numero);
                }
                else {
                    n.setValor(0.5f);
                }
            }
        }     
       
//Comenzamos a jugar
        
        //Acumulador para la puntuacion
        float puntuacionJugador = 0;
        float puntuacionAlaVista = 0;
        float puntuacionOculta = 0;
        //Acumulador para la puntuacion de la banca
        float puntuacionBanca = 0;
        //Si se planta o no; s ó n 
        char plantar = 'N';
        //Si oculta la carta o no; s ó n
        char ocultar = 'N';
        
        System.out.println("Juegas tú:");
        System.out.println("---------------------------------------------");    
        
        do {
            Naipe carta = b.repartir();
            System.out.println("\t\t" + carta);
            puntuacionJugador = puntuacionJugador + carta.getValor();
            
            //Si se pasa
            if (puntuacionJugador > 7.5f) {               
                break;
            }
                                   
            ocultar = ocultarCarta();
            
            if (ocultar == 'S') {
                puntuacionOculta = carta.getValor();
                puntuacionAlaVista = puntuacionJugador - puntuacionOculta;
                
                System.out.println("Tu puntuación a la vista es " + puntuacionAlaVista); 
            }
            else {
                puntuacionAlaVista = puntuacionJugador - puntuacionOculta;
                System.out.println("Tu puntuación a la vista es " + puntuacionAlaVista);
            }            
            
            //Damos al jugador la posibilidad de elegir que carta ocultar si consigue 7.5
            if (puntuacionJugador == 7.5f) {               
                break;
            }
            System.out.print("Tu puntuación es " + puntuacionJugador + ". ");
            
            plantar = plantarse();
            
        } while(plantar == 'N');
        
        System.out.println("La puntuación obtenida es " + puntuacionJugador);
        //si nos hemos pasado
        if (puntuacionJugador > 7.5f) {   
            System.out.println("Te has pasado. Has perdido.");
        }
        //si no nos hemos pasado juega la banca        
        else {
            System.out.println("\n\nJuega la banca:");
            System.out.println("---------------------------------------------");
            do {
                Naipe carta = b.repartir();
                System.out.println("\t\t" + carta);
                puntuacionBanca = puntuacionBanca + carta.getValor();
                //Si se pasa
                if (puntuacionBanca > 7.5f) {               
                    System.out.println("La banca se ha pasado con " + puntuacionBanca + ". Has ganado.");
                    break;
                }
                //Si la suma de las cartas a la vista del jugador es menor que seis y su puntación es mayor o igual a seis.
                if (puntuacionAlaVista < 6.0f && (puntuacionBanca >= 6.0f)) {
                    break;
                } 
                //Si la suma de las cartas a la vista del jugador es mayor o igual a seis y su puntuación es mayor o igual a dicha suma más 0,5.
                if (puntuacionAlaVista >= 6.0f && puntuacionBanca >= (puntuacionAlaVista + 0.5f)) {
                    break;
                }
                //En el caso que el jugador tenga 7.5 y no haya ocultado ninguna carta, la banca seguiría jugando si tiene 7.5. 
                //Para evitar esto la sacamos del bucle si tiene 7.5
                if (puntuacionBanca == 7.5f) {
                    break;
                }
                
            } while(true); 
            
            if (puntuacionBanca >= puntuacionJugador && puntuacionBanca <= 7.5f) {
                System.out.println("La banca se planta y gana. Su puntuación es " + puntuacionBanca);
            }
            if (puntuacionBanca < puntuacionJugador && puntuacionBanca < 7.5f) {
                System.out.println("La banca se planta y pierde. Su puntuación es " + puntuacionBanca + " y la del jugardor " + puntuacionJugador + ". ¡ENHORABUENA!");
            }
        }
    }
    
}
