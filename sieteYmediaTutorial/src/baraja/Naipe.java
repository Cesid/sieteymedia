package baraja;

/**
 *
 * @author CGG
 */
public class Naipe {

    //Atributos
    private int numero;
    private int palo;
    private float valor;
    private boolean repartido; 
        
    //Constructores
    public Naipe(int palo, int numero, float valor) {
        this.numero = numero;
        this.palo = palo;
        this.valor = valor;
        this.repartido = false;
    } 
        
    //Métodos

    public String getNumero() {
        switch (numero) {
            case 1:
                return "As";
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                return Integer.toString(numero);
            case 10:
                return "Sota";
            case 11:
                return "Caballo";
            default:
                return "Rey";
        }
    }

    public String getPalo() {
        switch (palo) {
            case Baraja.OROS:
                return "Oros";
            case Baraja.COPAS:
                return "Copas";
            case Baraja.ESPADAS:
                return "Espadas";
        }
        return "Bastos";
    }

    @Override
    public String toString() {
        return getNumero() + " de " + getPalo();
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public boolean isRepartido() {
        return repartido;
    }

    public void setRepartido(boolean repartido) {
        this.repartido = repartido;
    }
    
    
    
    
}
