package baraja;

/**
 *
 * @author CGG
 */
public class Baraja {
    
    //Constantes
    public static final int OROS = 0;
    public static final int COPAS = 1;
    public static final int ESPADAS = 2;
    public static final int BASTOS = 3;  
    
    public static final int AS = 1;
    public static final int DOS = 2;
    public static final int TRES = 3;
    public static final int CUATRO = 4;
    public static final int CINCO = 5;
    public static final int SEIS = 6;
    public static final int SIETE = 7;
    public static final int OCHO = 8;
    public static final int NUEVE = 9;
    public static final int SOTA = 10;
    public static final int CABALLO = 11;
    public static final int REY = 12;
    
    //Atributos
    private Naipe[][] baraja;
    private boolean con8y9;
    private int mazo;
    
    //Constructor
    public Baraja(boolean con8y9) {
        if (con8y9) {
            this.baraja = new Naipe[4][12];
            this.mazo = 48;
        }
        else {
            this.baraja = new Naipe[4][10];
            this.mazo = 40;
        }
        this.con8y9 = con8y9; 
        //Inicializamos el array de naipes
        crearNaipes();
    }
    
    //Métodos    
    private void crearNaipes() {
        for(int p = OROS; p <= BASTOS; p++) {
            for(int n = 0; n < mazo/4; n++) {
                int numero = n+1;
                switch (numero) {
                    case AS:
                    case DOS:
                    case TRES:
                    case CUATRO:
                    case CINCO:
                    case SEIS:
                    case SIETE:               
                        baraja[p][n] = new Naipe(p, numero, 0);
                        break;
                    case OCHO:
                        if(con8y9) {
                            baraja[p][n] = new Naipe(p, Baraja.OCHO ,0);
                        }
                        else {
                            baraja[p][n] = new Naipe(p, Baraja.SOTA ,0);
                        }
                        break;
                    case NUEVE:
                        if(con8y9) {
                            baraja[p][n] = new Naipe(p, Baraja.NUEVE ,0);
                        }
                        else {
                            baraja[p][n] = new Naipe(p, Baraja.CABALLO ,0);
                        }
                        break;
                     case SOTA:
                        if(con8y9) {
                            baraja[p][n] = new Naipe(p, Baraja.SOTA ,0);
                        }
                        else {
                            baraja[p][n] = new Naipe(p, Baraja.REY ,0);
                        }
                        break;
                    case CABALLO:
                        baraja[p][n] = new Naipe(p, Baraja.CABALLO ,0);
                        break;
                    case REY:
                        baraja[p][n] = new Naipe(p, Baraja.REY ,0);
                        break;
                }
            }
        }        
    }

    public int getMazo() {
        return mazo;
    }
    
    public Naipe getNaipe(int palo, int numero) {
        return baraja[palo][numero-1];
    }

    public boolean isCon8y9() {
        return con8y9;
    }
            
    public Naipe repartir() {
        if (mazo > 0) {
            int num;
            int pal;
            Naipe n;
            do {
                if (con8y9) {
                    num = (int)(Math.random() * 12) + 1;
                }
                else {
                    num = (int)(Math.random() * 10) + 1;
                }
                pal = (int)(Math.random() * 4);
                n = getNaipe(pal, num);
            } while(n.isRepartido());
            n.setRepartido(true);
            mazo--;
            return n;
        }
        else {
            return null; //Si el mazo está vacío
        }
    }

    @Override
    public String toString() {
        String b ="";
        int nCartas;
        if(con8y9 == true) {
            b = b + "Baraja de 48 cartas\n";
            nCartas = 12;
        }
        else {
            b = b + "Baraja de 40 cartas\n";
            nCartas = 10;
        }
        
        b = b + "Cartas sin repartir=" + mazo + "\n";
        
        for (int p = OROS; p <= BASTOS; p++) {
            for (int num = 1; num <= nCartas; num++) {
                b = b + this.getNaipe(p, nCartas) + " valor = " + this.getNaipe(p, nCartas).getValor() + "\n";
            }
        }        
        return b;
    }
    
    
}
